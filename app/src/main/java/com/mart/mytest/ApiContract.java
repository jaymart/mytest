package com.mart.mytest;

import java.util.List;

public interface ApiContract {

    interface View {

        void notifyProductAdded(boolean status);

        void notifyProductDeleted(boolean status);

        void onProductChanged(List<Product> myProducts);
    }

    interface Presenter {
        void addProduct(Product product);

        void deleteProduct(Product product);

    }
}

package com.mart.mytest;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ApiPresenter implements ApiContract.Presenter {

    private final ApiContract.View mApiView;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("products");

    public ApiPresenter(@NonNull ApiContract.View apiView){
        mApiView = apiView;

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                List<Product> myProducts = new ArrayList<>();
                for (DataSnapshot userData : dataSnapshot.getChildren()) {
                    Product product = userData.getValue(Product.class);
                    myProducts.add(product);
                    Log.e("zzz", product.toString()+"zz");
                }
                apiView.onProductChanged(myProducts);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) { }
        });
    }

    @Override
    public void addProduct(Product product) {
        String key = myRef.push().getKey();
        product.setId(key);
        assert key != null;
        myRef.child(key).setValue(product);
        mApiView.notifyProductAdded(true);
    }

    @Override
    public void deleteProduct(Product product) {
        myRef.child(product.getId()).getRef().removeValue();
        mApiView.notifyProductDeleted(true);
    }
}

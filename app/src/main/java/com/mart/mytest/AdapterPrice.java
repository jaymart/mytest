package com.mart.mytest;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;


public class AdapterPrice extends RecyclerView.Adapter<AdapterPrice.MyViewHolder> {

    private List<Product> productList;
    private ClickListener handleClickListener;

    interface ClickListener {
        void onClick(Product product);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
         TextView tvTitle, tvPrice;

        MyViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvPrice = itemView.findViewById(R.id.tv_price);
        }
    }

    AdapterPrice(List<Product> productList, ClickListener clickListener){
        super();
        this.productList = productList;
        handleClickListener = clickListener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Product product = productList.get(position);
        holder.tvTitle.setText(product.getTitle());
        holder.tvPrice.setText(String.valueOf(product.getPrice()));

        holder.itemView.setOnClickListener(view -> handleClickListener.onClick(product));

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

}

package com.mart.mytest;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterPrice.ClickListener, ApiContract.View {

    private Context mContext;
    private ApiPresenter apiPresenter;
    private AdapterPrice adapterPrice;
    private List<Product> myProducts = new ArrayList<>();
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;
        apiPresenter = new ApiPresenter(this);

        Button btnAdd = findViewById(R.id.btn_add_item);
        RecyclerView rvProduct = findViewById(R.id.rv_product);
        Button btnLogout = findViewById(R.id.btn_logout);

        btnAdd.setOnClickListener(view -> showAddDialog());

        rvProduct.setLayoutManager(new LinearLayoutManager(this));
        rvProduct.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapterPrice = new AdapterPrice(myProducts, this);
        rvProduct.setAdapter(adapterPrice);

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();

        btnLogout.setOnClickListener(view -> signOut());
    }

    private void signOut() {
        // Firebase sign out
        mAuth.signOut();

        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener(this, task -> {
            startActivity(new Intent(mContext, LoginActivity.class));
            finish();
        });
    }

    private void showAddDialog(){
        Dialog dialogAdd = new Dialog(mContext);
        dialogAdd.setContentView(R.layout.dialog_add);

        Window window = dialogAdd.getWindow();
        assert window != null;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        EditText etTitle = dialogAdd.findViewById(R.id.et_title);
        EditText etPrice = dialogAdd.findViewById(R.id.et_price);
        Button btnAdd = dialogAdd.findViewById(R.id.btn_add);

        btnAdd.setOnClickListener(view -> {

            String title = etTitle.getText().toString().trim();
            String price = etPrice.getText().toString().trim();

            if (title.equals("") || price.equals(""))
                return;

            apiPresenter.addProduct(new Product(title, Integer.parseInt(price)));
            dialogAdd.dismiss();
        });

        dialogAdd.show();
    }

    @Override
    public void onClick(Product product) {
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(mContext);
        deleteDialog.setTitle("Delete")
                .setMessage("Do you want to delete product "+product.getTitle())
                .setPositiveButton("Yes", (dialogInterface, i) -> apiPresenter.deleteProduct(product))
                .setNegativeButton("No", (dialogInterface, i) -> { })
                .show();

    }

    @Override
    public void notifyProductAdded(boolean status) {
        if (status)
            Toast.makeText(mContext, "Successfully Added", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void notifyProductDeleted(boolean status) {
        if (status)
            Toast.makeText(mContext, "Successfully Deleted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProductChanged(List<Product> myProducts) {
        this.myProducts.clear();

        if (myProducts.size() != 0)
            this.myProducts.addAll(myProducts);

        adapterPrice.notifyDataSetChanged();
    }
}
